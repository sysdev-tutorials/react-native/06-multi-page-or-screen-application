# Multi-page or screen application​

The main theme for this lesson is writing an application with multiple pages or screens. 
We will also focus on navigation back and forth between screens with and/or without sending data as parameters.

As before, we will continue improving our work-in-progress app as we learn new stuffs!

## Preparations
If you do a fresh start of a repo, copy inft2508 folder from previous lesson (https://gitlab.com/sysdev-tutorials/react-native/06-multi-page-or-screen-application/-/tree/main/inft2508) to your repo!

Recap: Af this stage, if you run the app, it should run fine with following features implemented
- Renders main/home screen with list of clickable 'Card' components
- Upon click on 'Food' card, list of restaurants are displayed in another screen
- Note that navigation from Restaurants page to main page is not implemnted yet!!

Then we are good to go!

## React Navigation - introduction and installation
In order to have support for multiple screens and nagivation between screen in an app, we are going to communitiy developed library called ```React Nagivation```. See official documentation here 
 - On react navigation website https://reactnavigation.org/docs/getting-started/
 - On react native website https://reactnative.dev/docs/navigation#react-navigation

Note that it is not part of official react-native package. This means that we need to install and use it as third party library.

### Installation of react navigation and its dependencies
Readers are recommended to follow the official dopcumentation above. In the following only summary of installations will be given.

(Note - the following commands are tested only on Mac OS. For other OS do your own research)

### Install react-navigation​

First let´s install native and native-stack packages of react-nagivation. Native package contains common elements for different other parts of react-navigation. The native-stack package basically provides stack based navigation of screens that we will use in the following section of this lesson.

```
npm install @react-navigation/native @react-navigation/native-stack​
```

Then install other dependencies that react-navigation depends on.

```
npm install react-native-screens react-native-safe-area-context​
```

Finally if you working on iOS project/platform, you need to run the following command as well from the project folder.

```
cd ios && pod install
```

After all these, inspect package.json file and make sure that these are listed in the dependencies section, like this. Note that their versions can be different that listed here!

```
  "dependencies": {
    "@react-navigation/native": "^6.0.11",
    "@react-navigation/native-stack": "^6.7.0",
    ...
    "react-native-safe-area-context": "^4.3.1",
    "react-native-screens": "^3.15.0"
  },
```

## Usage of react navigation
Using react navigation is actually quite straight forward. Basically all that needs to be done is this

<img align="right" src="react_navigation_basic.png" width=500>

- First, import ```NavigationContainer``` component from ```@react-navigation/native``` package​
- Then wrap the whole app in ```NavigationContainer``` component

<br clear="both">


### Wrapping application within ```NavigationContainer``` component

Within ```NavigationContainer``` we need to define a 'Navigator' component type from react-navigation library. There are different types of 'Nagivators' offered by the  package and they should be chosen/used depending on the needs. Note that different package may need to be installed for their usage. Some examples of different types of 'Navigators' are​ following.

- StackNavigator: this provides a way for an app to transition between screens where each new screen is placed on top of a stack. More can be read here https://reactnavigation.org/docs/stack-navigator/​

- TabNavigator: this provides tabs on the bottom of the screen or on the top below the header (or even instead of a header). More on this can be read here https://reactnavigation.org/docs/tab-based-navigation/​

- DrawerNavigator: this type of navigator uses drawer from left (or right) side for navigating between screens. More on this can be read here https://reactnavigation.org/docs/drawer-based-navigation​

In this lesson, we will only be using ```StackNavigator```. Readers are encouraged to tryout other type of Nagivators on their own!​


### Using StackNavigator for navigation between pages
In order to use 'StackNavigator', the following steps need to be done

- Import ´createNativeStackNavigator´ function from '@react-navigation/native-stack' package
- Then define a ´Stack´ (name can be anything) by calling 'createNativeStackNavigator' function
- Then use 'Stack.Navigator' component as child to the ´NavigationContainer´.
- Then use one or more 'Stack.Screen' components as chile to 'Stack.Navigator' component. Note that the 'Stack.Screen' component takes couple of parameters, and two of the mandatory parameters are 'name' and 'component'.
  - 'name' parameter represents name to be used for the screen which is used to navigate to the screen. Note that name can be any string!
  - 'component' param represents the React Component to render for the screen! It can for example be Home page or other pages. In the following we will write our own React components representing 'screens' and then assign them to this 'component' paramenter!


The code snippet below show priliminary (not runnable) contents for our App.js file. It is not runnable because we have not implemented 'HomeScreen' and 'FoodItemsScreen' yet!

```
## App.js file contents

import React, {useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/Home';
import FoodItemsScreen from './screens/FoodItems';

const Stack = createNativeStackNavigator();

const Inft2508App = () => {
  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          />
        <Stack.Screen
          name="FoodItems"
          component={FoodItemsScreen}
        />
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Inft2508App;
```

Next, let´s first create skeletons for 'HomeScreen' and 'FoodItemsScreen'! Details will be implemented in the next sections. Create a 'screens' folder and under that create ´Home.js´ and ´FoodItems.js´ file with following contents.

<img align="right" src="stack_vabigator_template.png" width=300>

```
// contents for screens/Home.js file
import React, {useState} from 'react';
import { SafeAreaView } from 'react-native';

const HomeScreen = ({navigation}) => {
    return (
        <SafeAreaView>
            {
            // contents to be added
            }
        </SafeAreaView>
    );
}

export default HomeScreen;


// contents for screens/FoodItems.js file
import React, {useState} from 'react';
import { SafeAreaView } from 'react-native';

const FoodItemsScreen = ({navigation, route}) => {
    return (
        <SafeAreaView>
             {
                // contents to be added
             }
        </SafeAreaView>
    );
}

export default FoodItemsScreen;
```
 <br clear="both">

These are the skeleton for our 'HomeScreen' and 'FoodItemsScreen'. When the application is run, it shows a top navigation bar and empty home screen.

Note the screen components are similar to usual react components execpt that each screen component is provided with navigation related properties such as 'navigation', 'route', etc. These navigation properties are used for navigating back and forth between the screens. We will see the details usecases later in this lesson.

### Implementing our 'HomeScreen' react component
Now, let´s do proper/coomplete implementation of our 'HomeScreen'!

We do this by carving out main UI related code from the 'App.js' from previous lesson. That basically contains the following

- keep import statements mostly as before
- Crav out 'logos', 'initialState' constants as they are. 
- Set 'cards' state variable as it is
- Within 'showCardItems' function, the logic should be such that it should nagivate to the another page i.e. FoodItems page. This is done by calling 'navigate' function of 'navigation' property which is available to the screen compponent. Like this ``` navigation.navigate("FoodItems")```.
- Under 'return' block, take away inline ```if...&&``` statements, keep 'SafeAreaView' view and its two child 'View' elements - one representing UI for search box and another representing scrollable 'Card' components.

The resulting contents for 'App.js' file are following. If you run the app now, you should see Home screen is populated with pressable Card components. Top navigation is there. Each 'Card' items are pressable and on press an empty screen will be shown because FoodItemsScreen does not contain any displayable UI elements yet. Note also that the navigation bar will have a back menu with 'Home' text and that will navigate the page back to home screen. 

There you go, now you can navigate back and forth between the screens.

![](home_1.png)

```
import React, {useState} from 'react';
import { 
    View, 
    TextInput, 
    ScrollView, 
    SafeAreaView
  } from 'react-native';
  import Card from '../components/Card'

const HomeScreen = ( {navigation} ) => {
    const logos = {
        logo_music: require('../images/logo-music.png'),
        logo_vechiles: require('../images/logo-vechiles.png'),
        logo_food: require('../images/logo-food.png'),
        logo_realstate: require('../images/logo-realstate.jpeg'),
        logo_dating: require('../images/logo-dating.png'),
        logo_mobile: require('../images/logo-mobile.png'),
      };
  
      // initial state
      const initialState = [
        {id: 1, displayText: "Music", logo: logos.logo_music},
        {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
        {id: 3, displayText: "Food", logo: logos.logo_food},
        {id: 4, displayText: "Real State", logo: logos.logo_realstate},
        {id: 5, displayText: "Dating", logo: logos.logo_dating},
        {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
        {id: 7, displayText: "Music", logo: logos.logo_music},
        {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
        {id: 9, displayText: "Food", logo: logos.logo_food},
        {id: 10, displayText: "Real State", logo: logos.logo_realstate},
        {id: 11, displayText: "Dating", logo: logos.logo_dating},
        {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
        {id: 13, displayText: "Music", logo: logos.logo_music},
        {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
        {id: 15, displayText: "Food", logo: logos.logo_food},
        {id: 16, displayText: "Real State", logo: logos.logo_realstate},
        {id: 17, displayText: "Dating", logo: logos.logo_dating},
        {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
      ]
  
      // set initial states
      const [cards, setCards] = useState (initialState);

      const showCardItems =  (category) => {
        // navigate to items page
        navigation.navigate("FoodItems")
      }

    return (
        <SafeAreaView>
            <View style= {{ margin: 10, alignItems: "center" }}>
                <TextInput
                placeholder="Type here to search!"
                onChangeText={newText => {
                    var matchedCards = [];
                    // apply search only if search string length >= 2
                    if(newText.trim().length >= 2) {
                    initialState.map((value, index) => {
                        if(value.displayText.includes(newText)) {
                        matchedCards.push(value);
                        }
                    });
                    setCards(matchedCards);
                    } else{
                    setCards(initialState);
                    }
                }}
                />
            </View>
                <ScrollView>
                <View style={{ 
                    flex: 1, 
                    justifyContent: "center", 
                    alignItems: "center",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    alignContent: "center",
                    }}>

                    {
                        cards.map((value, index) => {
                            return <Card key={value.id} displayText={value.displayText} logo={value.logo} showItems={showCardItems}/>
                        }
                        )
                    }
                </View>
                </ScrollView>
            </SafeAreaView>
    );
}

export default HomeScreen;
```


### Implementing our 'FoodItemsScreen' react component
Not let´s carv out FoodItems (list of restaurants) related UI from the 'App.js' file from previous lesson into a new file 'screens/FoodItems.js'. Basically this invold the following steps. Note that we will simplify some stuffs for convinience!

- keep related import statements
- keep 'items' state variable and initialize with restaurants data (which is read from dummydata.json file)
 - under 'return' block, take away inline ```if...&&``` statements, keep 'SafeAreaView' view as container and as its child keep the 'View' element that dispayes list of restaurants via 'SectionList' element. 
 - Note that we will remove the 'View' element containing search box!. Readers are encourages to put it back with modified 'search' logic later in this lesson when react navigation concepts are better understood!

 So, let´s see how the preliminary version of 'screens/FoodItems.js' file looks like.

If you run the app and click on 'Food' card in the 'Home' screen you will now see that it goes to another screen, the 'FoodItemsScreen' and list of restautants are shown. Note also top navigation bar with 'Back' menu is still there.

<img align="right" src="food_items_screen_1.png" width=200>

```
import React, {useState} from 'react';
import { 
    View, 
    SafeAreaView,
    SectionList,
    Text,
  } from 'react-native';


const FoodItemsScreen = ( {navigation, route} ) => {

    var data = require('../data/dummydata.json');
    const [items, setItems] = useState (data.restaurants);

    return (
        <SafeAreaView>
            <View style={{
                paddingTop: 20}}>
                    <SectionList 
                    sections={items}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({item}) => (
                        <View style={{
                            backgroundColor: "#D3FFF6", 
                            padding: 10, marginVertical: 2, 
                            flexDirection: "row", 
                            justifyContent: "space-between"}}>
                            <Text style={{fontSize: 18}}>{item.menu}</Text>
                            <Text style={{fontSize: 18}}>{item.price} nok</Text>
                        </View>
                        )}
                    renderSectionHeader={({ section: { name, address } }) => (
                        <View>
                            <Text style={{fontSize: 24, backgroundColor: "#fff"}}>{name}</Text>
                            <Text style={{fontSize: 18, backgroundColor: "#fff"}}>{address}</Text>
                        </View>
                    )}
                    />
            </View>
        </SafeAreaView>
    );
}

export default FoodItemsScreen;
```

<br clear="both">

### Passing data betwen the screens
In our app, we can now move back and forth between the screens. However, if you have not noticed yet, there are few serious flaws. Some to mention are followng

- On click of every 'Card' components (not only Food) in the 'Home' screen leads to 'FoodItemsScreen'. For example it will display list of restaurant evenif you click on 'Vechiles' components and so on!
- Within 'FoodItemsScreen' component, restaurants data is always read from the 'dummydata.json' file and 'items' state variable is populated

No worries, such implementation was intentional! We will solve this problem with the help of passing relavant data to the target screen when user navigates from one screen to another. Let´s see how this can be done!

This is actually quite simple! All you need to do is following
- pass additional parameters when you call 'navigate' function on 'navigation' property in the source screen,

  ```navigation.navigate('RouteNme', {/* parameters with values go here */})```. 
- and then retrieve these parameters as route parameters on the taraget screen, 

  ```const { /* params defined here */} = route.params```.

Let´s then apply this approach and modifiy our 'Home' and 'FoodItems' screen such that whenver a 'Card' item is pressed on 'Home' screen we will send associated 'category' to the target 'FoodItems' screen. In the 'FoodItems', we will retrieve the value of the 'category' from route parameter. Then only read the dummydata.json file and update the 'items' state variable only if the catetory is 'Food'. That means if other than 'food' category is selected in the home screen, no items will be displayed in the 'FoodItems' screen.

Let´s the code snippet of updated 'Home.js' and 'FoodItems.js' file.

```
// Contents of Home.js file

import React, {useState} from 'react';
import { 
    View, 
    TextInput, 
    ScrollView, 
    SafeAreaView
  } from 'react-native';
  import Card from '../components/Card'

const HomeScreen = ( {navigation} ) => {
    const logos = {
        logo_music: require('../images/logo-music.png'),
        logo_vechiles: require('../images/logo-vechiles.png'),
        logo_food: require('../images/logo-food.png'),
        logo_realstate: require('../images/logo-realstate.jpeg'),
        logo_dating: require('../images/logo-dating.png'),
        logo_mobile: require('../images/logo-mobile.png'),
      };
  
      // initial state
      const initialState = [
        {id: 1, displayText: "Music", logo: logos.logo_music},
        {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
        {id: 3, displayText: "Food", logo: logos.logo_food},
        {id: 4, displayText: "Real State", logo: logos.logo_realstate},
        {id: 5, displayText: "Dating", logo: logos.logo_dating},
        {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
        {id: 7, displayText: "Music", logo: logos.logo_music},
        {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
        {id: 9, displayText: "Food", logo: logos.logo_food},
        {id: 10, displayText: "Real State", logo: logos.logo_realstate},
        {id: 11, displayText: "Dating", logo: logos.logo_dating},
        {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
        {id: 13, displayText: "Music", logo: logos.logo_music},
        {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
        {id: 15, displayText: "Food", logo: logos.logo_food},
        {id: 16, displayText: "Real State", logo: logos.logo_realstate},
        {id: 17, displayText: "Dating", logo: logos.logo_dating},
        {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
      ]
  
      // set initial states
      const [cards, setCards] = useState (initialState);

      const showCardItems =  (category) => {
        // navigate to items page
        navigation.navigate("FoodItems", {category: category})
      }

    return (
        <SafeAreaView>
            <View style= {{ margin: 10, alignItems: "center" }}>
                <TextInput
                placeholder="Type here to search!"
                onChangeText={newText => {
                    var matchedCards = [];
                    // apply search only if search string length >= 2
                    if(newText.trim().length >= 2) {
                    initialState.map((value, index) => {
                        if(value.displayText.includes(newText)) {
                        matchedCards.push(value);
                        }
                    });
                    setCards(matchedCards);
                    } else{
                    setCards(initialState);
                    }
                }}
                />
            </View>
                <ScrollView>
                <View style={{ 
                    flex: 1, 
                    justifyContent: "center", 
                    alignItems: "center",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    alignContent: "center",
                    }}>

                    {
                        cards.map((value, index) => {
                            return <Card key={value.id} displayText={value.displayText} logo={value.logo} showItems={showCardItems}/>
                        }
                        )
                    }
                </View>
                </ScrollView>
            </SafeAreaView>
    );
}

export default HomeScreen;
```

```
// Contents of FoodItems.js

import React, {useState} from 'react';
import { 
    View, 
    SafeAreaView,
    SectionList,
    Text,
  } from 'react-native';


const FoodItemsScreen = ( {navigation, route} ) => {

    const {category} = route.params;

    var foodItems = [];
    if (category == "Food") {
        var data = require('../data/dummydata.json');
        foodItems = data.restaurants;
    }
    const [items, setItems] = useState (foodItems);

    return (
        <SafeAreaView>
            <View style={{
                paddingTop: 20}}>
                    <SectionList 
                    sections={items}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({item}) => (
                        <View style={{
                            backgroundColor: "#D3FFF6", 
                            padding: 10, marginVertical: 2, 
                            flexDirection: "row", 
                            justifyContent: "space-between"}}>
                            <Text style={{fontSize: 18}}>{item.menu}</Text>
                            <Text style={{fontSize: 18}}>{item.price} nok</Text>
                        </View>
                        )}
                    renderSectionHeader={({ section: { name, address } }) => (
                        <View>
                            <Text style={{fontSize: 24, backgroundColor: "#fff"}}>{name}</Text>
                            <Text style={{fontSize: 18, backgroundColor: "#fff"}}>{address}</Text>
                        </View>
                    )}
                    />
            </View>
        </SafeAreaView>
    );
}

export default FoodItemsScreen;
```


That´s it for this lesson. 


If you are interested feel free to modify the dataset and add seperate screens for each categories and add navigation logic to navigate among them!



### That´s it for this lesson.
In this lesson we have basically learned to use some key features of react-navigation that provides multi screen and navigation support!

In the next lesson, we will shift our focus 'data'. We will see how mobile application can be retrieve and update data via REST API. We will also check possibilities of data persistance on local device storage.


## Exercise #5
This exercise will also be a follow-up exercise from previous exercises. The key tasks for this exercise are following
 - Refactor previous exercise (https://gitlab.com/sysdev-tutorials/react-native/05-basic-react-components-iii#exercise-4) in such a way that you now will have multiple (at least 3) screen components
 - Use at least one of the Navigator types provided by react-navigation to have navigation between the implemented screens 
 - Pass parameters when navigating from one screen to another and make use of them

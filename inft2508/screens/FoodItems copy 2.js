import React, {useState} from 'react';
import { 
    View, 
    SafeAreaView,
    SectionList,
    Text,
  } from 'react-native';


const FoodItemsScreen = ( {navigation, route} ) => {

    var data = require('../data/dummydata.json');
    const [items, setItems] = useState (data.restaurants);

    return (
        <SafeAreaView>
            <View style={{
                paddingTop: 20}}>
                    <SectionList 
                    sections={items}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({item}) => (
                        <View style={{
                            backgroundColor: "#D3FFF6", 
                            padding: 10, marginVertical: 2, 
                            flexDirection: "row", 
                            justifyContent: "space-between"}}>
                            <Text style={{fontSize: 18}}>{item.menu}</Text>
                            <Text style={{fontSize: 18}}>{item.price} nok</Text>
                        </View>
                        )}
                    renderSectionHeader={({ section: { name, address } }) => (
                        <View>
                            <Text style={{fontSize: 24, backgroundColor: "#fff"}}>{name}</Text>
                            <Text style={{fontSize: 18, backgroundColor: "#fff"}}>{address}</Text>
                        </View>
                    )}
                    />
            </View>
        </SafeAreaView>
    );
}

export default FoodItemsScreen;
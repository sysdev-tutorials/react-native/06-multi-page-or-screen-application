import React from 'react';
import { Text, View, Image } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"}}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10,
          justifyContent: "center", 
          alignItems: "center",
        }}>
        <View style= {{flex: 1, justifyContent: "flex-end"}}>
          <Image
            style= {{width: 20, height: 20}}
            source={require("./logo.png")}
          />
        </View>
        <View style= {{flex: 1, justifyContent: "flex-start"}}>
          <Text>Hello!</Text>
        </View>
      </View>
    </View>
  );
}

export default Inft2508App;
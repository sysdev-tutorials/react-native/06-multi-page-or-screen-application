import React from 'react';
import { View } from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        flexDirection: "row",
        flexWrap: "wrap",
        alignContent: "center",
        }}>
        <Card displayText="Music" logo={logos.logo_music}/>
        <Card displayText="Vechiles" logo={logos.logo_vechiles}/>
        <Card displayText="Food" logo={logos.logo_food}/>
        <Card displayText="Real State" logo={logos.logo_realstate}/>
        <Card displayText="Dating" logo={logos.logo_dating}/>
        <Card displayText="Mobile" logo={logos.logo_mobile}/>
    </View>
  );
}

export default Inft2508App;
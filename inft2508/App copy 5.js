import React from 'react';
import { View } from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"}}>
        <Card/>
        <Card/>
        <Card/>
        <Card/>
    </View>
  );
}

export default Inft2508App;